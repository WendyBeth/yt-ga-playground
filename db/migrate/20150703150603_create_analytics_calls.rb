class CreateAnalyticsCalls < ActiveRecord::Migration
  def change
    create_table :analytics_calls do |t|
      t.integer :events_pulled
      t.timestamps null: false
    end
  end
end
