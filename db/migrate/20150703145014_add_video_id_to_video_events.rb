class AddVideoIdToVideoEvents < ActiveRecord::Migration
  def change
    add_column :video_events, :video_id, :integer
  end
end
