class CreateApiCalls < ActiveRecord::Migration
  def change
    create_table :api_calls do |t|
      t.string :page
      t.string :sort
      t.integer :limit

      t.timestamps null: false
    end
  end
end
