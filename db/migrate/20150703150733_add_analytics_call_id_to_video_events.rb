class AddAnalyticsCallIdToVideoEvents < ActiveRecord::Migration
  def change
    add_column :video_events, :analytics_call_id, :integer
  end
end
