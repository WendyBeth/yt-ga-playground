class AddApiCallIdToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :api_call_id, :integer
  end
end
