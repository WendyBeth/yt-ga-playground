class CreateVideoEvents < ActiveRecord::Migration
  def change
    create_table :video_events do |t|
      t.string :label
      t.string :action
      t.integer :total
      t.date :date
      t.timestamps null: false
    end
  end
end
