class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :slug
      t.string :title
      t.string :host
      t.string :embed_code
      t.timestamps null: false
    end
  end
end
