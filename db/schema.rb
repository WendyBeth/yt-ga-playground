# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150703150733) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "analytics_calls", force: :cascade do |t|
    t.integer  "events_pulled"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "api_calls", force: :cascade do |t|
    t.string   "page"
    t.string   "sort"
    t.integer  "limit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "video_events", force: :cascade do |t|
    t.string   "label"
    t.string   "action"
    t.integer  "total"
    t.date     "date"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "video_id"
    t.integer  "analytics_call_id"
  end

  create_table "videos", force: :cascade do |t|
    t.string   "slug"
    t.string   "title"
    t.string   "host"
    t.string   "embed_code"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "api_call_id"
  end

end
