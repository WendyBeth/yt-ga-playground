Rails.application.routes.draw do
  root 'home#index'

  resources :api_calls, only: [:new, :create] do
    resources :videos, only: [:new, :create]
  end

  resources :videos, only: [:index, :show]
  resources :events, only: [:index, :create]
end
