# Youtube-Google-Analytics Playground

This application is one large 'code spike' for a
[Confreaks.TV](http://confreaks.tv) project, which I will use to explore the
various problems and possible solutions involved in collecting, organizing, and
displaying analytics gathered from embedded Youtube videos, specifically for
[Confreaks.TV](http://confreaks.tv).

# Process

### Set up Google Analytics using the GA tracking script

Create an application in Google Analytics. Replace `UA-XXXXXXXX-X` with your project ID. Store the following script in a partial and reference it in the `<head>` of application.html.erb:

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-XXXXXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>

### Setup the Lunametrics Youtube-Google-Analytics script

Download the most recent version of the Lunametrics Youtube-Google-Analytics script from [its Github page](https://github.com/lunametrics/youtube-google-analytics) and put it in the asset pipeline. Replace "OPT_CONFIG_OBJ" at the end of the file with the following:

    {
      'forceSyntax': 1,         // forces Universal Analytics syntax
      'percentageTracking': 20  // Tracks every 20% viewed
    });

### Hook application into Google API as a service

Go to the Developer Console and create a new project. Under 'APIs and Auth', first ensure that the Analytics API is listed under Enabled APIs under 'APIs'. If it's not, enable it.

Then go to 'Credentials' and click 'Create new Client ID'. Select the option for 'Service Account' and download the .p12 file. Place it in your application, add it to .gitignore (VERY IMPORTANT).

Go to your Google Analytics account for the project. Under 'Admin', add your '###@developer.gserviceaccount.com' email address (provided on the same page where you download the .p12 key file) as an authorized user.

### Gems to interact with the Google Analytics API

Add:

    gem 'oauth2'
    gem 'google-api-client'

to the Gemfile. `bundle install`.

### The script for connecting to the API and returning information:

(Note: This is not the final implementation. This is a working example script which successfully authenticates the application, runs a query, and returns it.)

    GA_KEY_FILE = the .p12 key file you downloaded
    GA_EMAIL = the .gserviceaccount email address

    require 'google/api_client'

    client = Google::APIClient.new(application_name: 'Playground', application_version: '1')
    client.authorization = Signet::OAuth2::Client.new(
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
      audience: 'https://accounts.google.com/o/oauth2/token',
      scope: 'https://www.googleapis.com/auth/analytics.readonly',
      issuer: GA_EMAIL,
      signing_key: Google::APIClient::PKCS12.load_key(GA_KEY_FILE, 'notasecret')
    ).tap { |auth| auth.fetch_access_token! }

    api_method = client.discovered_api('analytics', 'v3').data.ga.get

    result = client.execute(api_method: api_method, parameters: {
      'ids' => 'ga:104082366',
      'start-date' => Date.new(2015,1,1).to_s,
      'end-date' => Date.today.to_s,
      'dimensions' => 'ga:eventCategory',
      'metrics' => 'ga:totalEvents'
    })

    puts result.data.rows

### Integrate this into the Rails app

In progress.

### The process for getting the PRIVATE_KEY and CERT files from the .pk12 file:

    openssl pkcs12 -in path.p12 -out newfile.crt.pem -clcerts -nokeys
    openssl pkcs12 -in path.p12 -out newfile.key.pem -nocerts -nodes