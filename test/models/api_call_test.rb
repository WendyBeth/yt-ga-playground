require 'test_helper'

class ApiCallTest < ActiveSupport::TestCase
  def setup
    @call = api_calls(:valid_call)
  end

  test "constructs url based on page, sort, and limit" do
    url = "http://confreaks.tv/api/v1/videos?sort=most_viewed&limit=1"
    assert_equal url, @call.construct_url
  end

  test "constructs url based on one parameter" do
    limit_url = "http://confreaks.tv/api/v1/videos?limit=2"
    sort_url = "http://confreaks.tv/api/v1/videos?sort=most_recent"
    assert_equal limit_url, api_calls(:multi_call).construct_url
    assert_equal sort_url, api_calls(:sorted_call).construct_url
  end

  test "constructs url properly even is sort is ''" do
    @call.update_attributes(sort: "")
    url = "http://confreaks.tv/api/v1/videos?limit=1"
    assert_equal url, @call.construct_url
  end

  test "get_result returns an array with at least one video object" do
    VCR.use_cassette("most_viewed_video_request") do
      obj = @call.get_result
      assert obj[0].has_key?("title")
    end
  end

  test "failure returns alert with constructed url" do
    fake_call = ApiCall.create(page: "fake")

    VCR.use_cassette("bad_request") do
      obj = fake_call.get_result
      assert_equal "api unavailable from #{fake_call.construct_url}", obj
    end
  end
end
