require 'test_helper'

class ConstructsVideoTest < ActiveSupport::TestCase
  test "turns JSON hash into Video object" do
    obj = {"title"=>"A Title", "slug"=>"a-slug",
            "host"=>"youtube", "embed_code"=>"aString"}

    constructor = ConstructsVideo.new(obj)
    constructor.build

    assert_equal obj["title"], constructor.video.title
    assert_equal obj["slug"], constructor.video.slug
    assert_equal obj["host"], constructor.video.host
    assert_equal obj["embed_code"], constructor.video.embed_code
  end
end
