require 'test_helper'

class ConstructsVideoEventTest < ActiveSupport::TestCase
  def setup
    @event_array = ["40%", "https://www.youtube.com/watch?v=gB-JSh1EVME", "20150622", "1"]
  end

  test "turns array into new VideoEvent" do
    constructor = ConstructsVideoEvent.new(@event_array)

    assert_equal VideoEvent, constructor.build.class
  end

  test "populates new VideoEvent with array items" do
    constructor = ConstructsVideoEvent.new(@event_array)
    event = constructor.build

    assert_not event.action.blank?
    assert_not event.label.blank?
    assert_not event.date.blank?
    assert_not event.total.blank?
  end

  test "extracts youtube embed code from label" do
    constructor = ConstructsVideoEvent.new(@event_array)
    embed_code = constructor.extract_embed_code

    assert_equal embed_code, "gB-JSh1EVME"
  end

  test "finds video_id for video event based on embed_code" do
    constructor = ConstructsVideoEvent.new(@event_array)
    video_id = constructor.find_video_id(constructor.extract_embed_code)

    assert_equal videos(:valid_video).id, video_id
  end

  test "populates new event video with the video it belongs to" do
    constructor = ConstructsVideoEvent.new(@event_array)
    video_event = constructor.build

    assert_equal videos(:valid_video).id, video_event.video_id
    assert_equal videos(:valid_video), video_event.video
  end

  test "creates new EventVideo" do
    constructor = ConstructsVideoEvent.new(@event_array)
    
    assert_difference('VideoEvent.count') do
      constructor.create!
    end
  end
end