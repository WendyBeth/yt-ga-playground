require 'test_helper'

class AnalyticsQuerierTest < ActiveSupport::TestCase

  test "default options set in AnalyticsQuerier" do
    querier = AnalyticsQuerier.new
    default_options = { 'ids'        => 'ga:104082366',
                        'start-date' => Date.new(2015,1,1).to_s,
                        'end-date'   => Date.today.to_s,
                        'metrics'    => 'ga:totalEvents'}
    assert_equal default_options, querier.options
  end

  test "can add non-required parameters to query" do
    querier = AnalyticsQuerier.new({'dimensions' => 'ga:eventCategory', 'end-date' => Date.new(2015,7,1).to_s})
    assert_equal 'ga:eventCategory', querier.options['dimensions']

    querier.options['filters'] = 'ga:eventCategory==Videos'
    assert querier.options.has_key?('filters')
  end

  test "can change defaults" do
    querier = AnalyticsQuerier.new({'metrics' => 'ga:uniqueEvents', 'end-date' => Date.new(2015,7,1).to_s})
    assert_equal 'ga:uniqueEvents', querier.options['metrics']
  end

  test "signs in with Google Analytics API" do
    VCR.use_cassette("successful_ga_authorization") do
      querier = AnalyticsQuerier.new('end-date' => Date.new(2015,7,1).to_s)
      client = querier.authorize_with_services

      assert_equal Google::APIClient, client.class
    end
  end

  test "queries analytics and returns results in rows (an array of arrays)" do
    # Scope variables to test so they're available both in and out of both VCR blocks
    querier, result = nil

    VCR.use_cassette("successful_ga_authorization") do
      querier = AnalyticsQuerier.new({'end-date' => Date.new(2015,7,1).to_s})
      querier.authorize_with_services
    end

    VCR.use_cassette("successful_ga_query") do
      result = querier.query_analytics
    end

    assert_equal Array, result[0].class
  end

  test "bad GA authorization response" do
    querier, result = nil

    VCR.use_cassette("bad_ga_authorization") do
      querier = AnalyticsQuerier.new({'end-date' => Date.new(2015,7,1).to_s})
      querier.authorize_with_services
    end

    assert_equal nil, querier.client

    result = querier.query_analytics
    message = "Could not access Google Analytics Reporting API due to authorization error"
    assert_equal message, result
  end

  test "attempted query with invalid query parameters" do
    querier, result = nil

    VCR.use_cassette("successful_ga_authorization") do
      querier = AnalyticsQuerier.new({'end-date' => Date.new(2015,7,1).to_s})
      querier.authorize_with_services
    end

    VCR.use_cassette("bad_ga_query") do
      querier.options['start-date'] = "INVALID START DATE"
      result = querier.query_analytics
    end

    assert_equal "Invalid query parameters.", result
  end
end