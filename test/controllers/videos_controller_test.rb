require 'test_helper'

class VideosControllerTest < ActionController::TestCase
  test "get new" do
    VCR.use_cassette("most_viewed_video_request") do
      get :new, api_call_id: api_calls(:valid_call)
      assert_response :success
      assert assigns(:videos)
    end
  end

  test "post create" do
    VCR.use_cassette("most_viewed_video_request") do
      assert_difference('Video.count') do
        post :create, api_call_id: api_calls(:valid_call)
      end

      assert_response :redirect
    end
  end

  test "post invalid create" do
    VCR.use_cassette("bad_request") do
      assert_no_difference('Video.count') do
        post :create, api_call_id: api_calls(:fake_call)
      end

      assert_response :redirect
    end
  end

  test "get index" do
    get :index
    assert_response :success
  end

  test "get show" do
    get :show, id: videos(:valid_video)
    assert_response :success
  end
end
