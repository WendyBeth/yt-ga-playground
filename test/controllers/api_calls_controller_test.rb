require 'test_helper'

class ApiCallsControllerTest < ActionController::TestCase
  test "get new" do
    get :new
    assert_response :success
  end

  test "post create" do
    VCR.use_cassette("most_viewed_video_request") do
      assert_difference('ApiCall.count') do
        post :create, api_call: { page: "videos", sort: "most_viewed",
                                  limit: 1 }
      end

      assert_redirected_to ("\/api_calls/#{ApiCall.last.id}/videos/new")
    end
  end

  test "post bad create" do
    VCR.use_cassette("bad_request") do
      assert_no_difference('ApiCall.count') do
        post :create, api_call: { page: "" }
      end

      assert_equal "Could not save API call.", flash[:alert]
      assert_template 'new'
    end
  end
end
