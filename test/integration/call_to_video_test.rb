require 'test_helper'

class CallToVideoTest < ActionDispatch::IntegrationTest
  test "from ApiCall#create to Video#new" do
    VCR.use_cassette("most_viewed_video_request") do
      assert_difference('ApiCall.count') do
        post_via_redirect '/api_calls',
                            api_call: { page: "videos",
                                        sort: "most_viewed",
                                        limit: 1 }
      end

      assert_equal "/api_calls/#{ApiCall.last.id}/videos/new", path
      assigns(:videos).each do |v|
        assert v.valid?
      end
    end
  end

  test "from ApiCall#create to Video#new with bad request" do
    VCR.use_cassette("bad_request") do
      assert_no_difference('ApiCall.count') do
        post_via_redirect '/api_calls', api_call: { page: "fake" }
      end

      assert_equal "That was a bad call.", flash[:alert]
      assert new_api_call_path, path
    end
  end
end
