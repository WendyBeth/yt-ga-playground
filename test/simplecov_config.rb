SimpleCov.start do
  add_filter '/test/'
  add_filter '/config/'

  add_group 'Actions', 'app/actions'
  add_group 'Controllers', 'app/controllers'
  add_group 'Helpers', 'app/helpers'
  add_group 'Mailers', 'app/mailers'
  add_group 'Models', 'app/models'
  add_group 'Services', 'app/services'
end
