class VideoEvent < ActiveRecord::Base
  belongs_to :video
  belongs_to :analytics_call
end