class Video < ActiveRecord::Base
  belongs_to :api_call
  has_many :video_events
  
  validates :title, presence: true
end
