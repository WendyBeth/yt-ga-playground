class AnalyticsCall < ActiveRecord::Base
  has_many :video_events
  validates :events_pulled, presence: true

  scope :last_call, -> { order(created_at: :desc).first }

  def get_batch


    querier = AnalyticsQuerier.new({

      'start-date' => AnalyticsCall.last_call_date.to_s,
      'end-date'   => Date.today.to_s,
      'metrics'    => 'ga:totalEvents',
      'dimensions' => 'ga:eventAction, ga:eventLabel, ga:date',
      'filters'    => 'ga:eventCategory==Videos'

      })

    querier.authorize_with_services
    batch = querier.query_analytics

    batch

  end

  def self.last_call_date
    if AnalyticsCall.count > 0
      last_call.created_at
    else
      Date.new(2015,1,1)
    end
  end

end
