class ApiCall < ActiveRecord::Base
  has_many :videos

  include HTTParty
  BASE_URI = "http://confreaks.tv/api/v1/"

  validates :page, presence: true

  def get_result
    JSON.parse(call_api.response.body)
  rescue
    "api unavailable from #{construct_url}"
  end

  def construct_url
    url = BASE_URI + page + determine_params
    url
  end

  def determine_params
    if sort || limit
      url = "?"
      if sort && limit && !sort.blank? && !limit.blank?
        url += "sort=#{sort}&limit=#{limit}"
      elsif sort && !sort.blank? && !limit
        url += "sort=#{sort}"
      elsif limit && !limit.blank? && (!sort || sort.blank?)
        url += "limit=#{limit}"
      end
    else
      url = ""
    end
    url
  end

  def call_api
    HTTParty::get(construct_url)
  end
end
