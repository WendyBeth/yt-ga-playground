module VideosHelper
  def embed(embed_code)
    source = "//youtube.com/embed/#{embed_code}?fs=1"
    content_tag(:iframe, nil, src: source)
  end
end
