require 'google/api_client'

class AnalyticsQuerier
  attr_accessor :options, :client

  GA_KEY_FILE  = ENV['GA_KEY_FILE']
  GA_EMAIL     = ENV['GA_EMAIL']
  GA_USER_ID   = '104082366'

  def initialize(options = {})
    @options    = options
    set_required_defaults

    @ids        = options["ids"]
    @start_date = options["start-date"]
    @end_date   = options["end-date"]
    @metrics    = options["metrics"]

    @dimensions = options["dimensions"] if options["dimensions"]
    @filters    = options["filters"] if options["filters"]
  end

  def query_analytics
    unless self.client.nil?
      api_method = self.client.discovered_api('analytics', 'v3').data.ga.get

      begin
      result = self.client.execute(api_method: api_method, parameters: options)
    rescue ArgumentError
      result = "Invalid query parameters."
    end

      if result == "Invalid query parameters."
        return result
      else
        return result.data.rows
      end
    else
      "Could not access Google Analytics Reporting API due to authorization error"
    end
  end

  def set_required_defaults
    self.options["ids"] = "ga:#{GA_USER_ID}"
    self.options["start-date"] ||= Date.new(2015,1,1).to_s
    self.options["end-date"]   ||= Date.today.to_s
    self.options["metrics"]    ||= "ga:totalEvents"
  end

  # Maybe we'll want to pull this out into a separate class or module eventually
  def authorize_with_services
    begin
    self.client = Google::APIClient.new(application_name: 'Playground', application_version: '1')

      p12 = OpenSSL::PKCS12.create('notasecret', 'descriptor',
                                   OpenSSL::PKey.read(ENV['PRIVATE_KEY']),
                                   OpenSSL::X509::Certificate.new(ENV['CERT']))

      p12_binary = p12.to_der

    self.client.authorization = Signet::OAuth2::Client.new(
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
      audience: 'https://accounts.google.com/o/oauth2/token',
      scope: 'https://www.googleapis.com/auth/analytics.readonly',
      issuer: GA_EMAIL,
      signing_key: Google::APIClient::PKCS12.load_key(p12_binary, 'notasecret')
    ).tap { |auth| auth.fetch_access_token! }
    rescue Signet::AuthorizationError
      self.client = nil
    end

    self.client
  end
end
