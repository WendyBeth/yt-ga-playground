class VideosController < ApplicationController
  def new
    @call_id = params[:api_call_id]
    @videos = []
    call = ApiCall.find(@call_id)
    results = call.get_result

    if results.class != String
      results.each do |result|
        constructor = ConstructsVideo.new(result)
        @videos << constructor.build
      end

      @videos.each do |video|
        video.api_call_id = @call_id
      end
    else
      flash[:alert] = "That was a bad call."
      ApiCall.find(@call_id).destroy
      redirect_to new_api_call_path
    end
  end

  def create
    @call_id = params[:api_call_id]
    @videos = []
    call = ApiCall.find(@call_id)
    results = call.get_result

    if results.class != String
      results.each do |result|
        constructor = ConstructsVideo.new(result)
        @videos << constructor.build
      end

      @videos.each do |video|
        video.api_call_id = @call_id
        video.save(video_params)
      end

      redirect_to videos_path and return
    else
      flash[:alert] = "That was a bad call!"
      ApiCall.find(@call_id).destroy
      redirect_to new_api_call_path
    end
  end

  def index
    @videos = Video.all
  end

  def show
    @video = Video.find(params[:id])
  end

  private

  def video_params
    params.permit(:title, :slug, :host, :embed_code, :api_call_id)
  end
end
