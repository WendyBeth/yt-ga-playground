class ApiCallsController < ApplicationController
  def new
    @api_call = ApiCall.new
  end

  def create
    @api_call = ApiCall.new(api_call_params)

    if @api_call.save
      flash[:notice] = "Call saved."
      redirect_to new_api_call_video_path(@api_call)
    else
      flash[:alert] = "Could not save API call."
      render 'new'
    end
  end

  protected

  def api_call_params
    params.require(:api_call).permit(:page, :sort, :limit)
  end
end
