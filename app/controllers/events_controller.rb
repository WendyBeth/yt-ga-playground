class EventsController < ApplicationController
  def index
    @video_events = VideoEvent.all
    @analytics_calls = AnalyticsCall.all
  end

  def create

    @analytics_call = AnalyticsCall.new
    batch = @analytics_call.get_batch

    @analytics_call.events_pulled = batch.count
    @analytics_call.save

    batch_id = @analytics_call.id

    batch.each do |video_event|
      constructor = ConstructsVideoEvent.new(video_event, batch_id)
      constructor.create!
    end

    redirect_to events_path

  end

end