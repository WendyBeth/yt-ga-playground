class ConstructsVideo
  attr_accessor :video, :obj

  def initialize(obj)
    @obj = obj
  end

  def build
    self.video = Video.new(vid_params)
    self.video
  end

  private

  def vid_params
    {
     title: obj["title"],
     slug: obj["slug"],
     host: obj["host"],
     embed_code: obj["embed_code"]
    }
  end
end
