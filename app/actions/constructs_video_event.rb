class ConstructsVideoEvent
  attr_accessor :event_array

  # Relying on the API return value being consistently organized
  # ["action", "label", "date", "total"]
  ACTION, LABEL, DATE, TOTAL = 0, 1, 2, 3

  def initialize(event_array, batch_id = nil)
    @event_array = event_array
    @batch_id = batch_id
  end

  def create!
    build.save
  end

  def build
    video_event = VideoEvent.new(action: event_array[ACTION],
                                 label: event_array[LABEL],
                                 date: event_array[DATE],
                                 total: event_array[TOTAL],
                                 video_id: find_video_id(extract_embed_code))

    video_event.analytics_call_id = @batch_id if @batch_id

    video_event
  end

  def extract_embed_code
    embed_string = event_array[LABEL]
    match = embed_string.match(/v=.*/)[0].gsub!("v=", "")
    match
  end

  def find_video_id(embed_code)
    Video.find_by(embed_code: embed_code).id
  end

end